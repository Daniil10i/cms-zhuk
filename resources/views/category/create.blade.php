@extends('layout.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Создать категорию </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Главная</a></li>
                        <li class="breadcrumb-item active">v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <form action="{{route('category.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="inputTitle">Введите название</label>
                        <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Категория">
                    </div>
                    <div class="form-group">
                        <label for="inputIdParent">Введите № родительской категории</label>
                        <input type="number" name="id_parent" class="form-control" id="inputIdParent" placeholder="1">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" id="submitCreateCategoryForm" value="Отправить">
                    </div>
                </form>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
